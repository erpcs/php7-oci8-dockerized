FROM php:7.1-apache

ADD instantclient_12_2.zip /tmp/

COPY docker-php.conf /etc/apache2/conf-enabled/docker-php.conf

RUN apt-get update && apt-get install -y --no-install-recommends apt-utils \
        openssl \
        libxrender1 \
        libfontconfig \
        libxext6 \
        unzip \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libmcrypt-dev \
        libpng-dev \
        htop \
        iotop \
        libaio1 \
        vim \
        libicu-dev \
        wget \
    && docker-php-ext-install -j$(nproc) iconv mcrypt gettext intl opcache \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install -j$(nproc) gd \
    && printf "log_errors = On \nerror_log = /dev/stderr\n" > /usr/local/etc/php/conf.d/php-logs.ini \
    && echo "[opcache]" > /usr/local/etc/php/conf.d/php_opcache.ini \
    && echo "opcache.memory_consumption=512" >> /usr/local/etc/php/conf.d/php_opcache.ini \
    && echo "opcache.interned_strings_buffer=64" >> /usr/local/etc/php/conf.d/php_opcache.ini \
    && echo "opcache.max_accelerated_files=32531" >> /usr/local/etc/php/conf.d/php_opcache.ini \
    && echo "opcache.validate_timestamps=1" >> /usr/local/etc/php/conf.d/php_opcache.ini \
    && echo "opcache.save_comments=1" >> /usr/local/etc/php/conf.d/php_opcache.ini \
    && echo "opcache.fast_shutdown=0" >> /usr/local/etc/php/conf.d/php_opcache.ini \
    && echo "realpath_cache_size=4096K" >> /usr/local/etc/php/conf.d/php_opcache.ini \
    && echo "realpath_cache_ttl=600" >> /usr/local/etc/php/conf.d/php_opcache.ini \
    && a2enmod rewrite \
    && unzip /tmp/instantclient_12_2.zip -d /usr/local/ \
    && ln -s /usr/local/instantclient_12_2/libclntsh.so.12.1 /usr/local/instantclient_12_2/libclntsh.so \
    && ln -s /usr/local/instantclient_12_2/sqlplus /usr/bin/sqlplus \
    && echo 'export LD_LIBRARY_PATH="/usr/local/instantclient_12_2"' >> /root/.bashrc \
    && echo 'export TNS_ADMIN="/usr/local/instantclient_12_2"' >> /root/.bashrc \
    && echo 'export ORACLE_BASE="/usr/local/instantclient_12_2"' >> /root/.bashrc \
    && echo 'export ORACLE_HOME="/usr/local/instantclient_12_2"' >> /root/.bashrc \
    && echo 'umask 002' >> /root/.bashrc \
    && echo 'instantclient,/usr/local/instantclient_12_2' | pecl install oci8 \
    && echo "extension=oci8.so" > /usr/local/etc/php/conf.d/php-oci8.ini \
    && echo "short_open_tag=Off" > /usr/local/etc/php/conf.d/short_open_tag.ini \
    && echo "memory_limit=256M" > /usr/local/etc/php/conf.d/php_memory.ini \
    && wget https://phar.phpunit.de/phpunit.phar -O /usr/local/bin/phpunit \
    && chmod +x /usr/local/bin/phpunit \
    && echo "<?php echo phpinfo(); " > /var/www/html/phpinfo.php \
    && echo "<?php " >> /var/www/html/clearCache.php \
    && echo "cache_reset(); " >> /var/www/html/clearCache.php \
    && echo "echo 'Ok';" >> /var/www/html/clearCache.php \
    && ln -snf /usr/share/zoneinfo/America/Bogota /etc/localtime && echo America/Bogota > /etc/timezone \
    && printf '[PHP]\ndate.timezone = "%s"\n', America/Bogota > /usr/local/etc/php/conf.d/tzone.ini \
    && echo 'alias c="php bin/console"' >> ~/.bashrc \
    && echo 'alias cc="php bin/console cache:clear"' >> ~/.bashrc \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /tmp/*

ENV LD_LIBRARY_PATH /usr/local/instantclient_12_2

RUN 'date'

EXPOSE 80
